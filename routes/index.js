module.exports = function (app, addon) {

    // Root route. This route will serve the `atlassian-connect.json` unless the
    // documentation url inside `atlassian-connect.json` is set
    app.get('/', function (req, res) {
        res.format({
            // If the request content-type is text-html, it will decide which to serve up
            'text/html': function () {
                res.redirect('/atlassian-connect.json');
            },
            // This logic is here to make sure that the `atlassian-connect.json` is always
            // served up when requested by the host
            'application/json': function () {
                res.redirect('/atlassian-connect.json');
            }
        });
    });

    // This is an example route that's used by the default "generalPage" module.
    // Verify that the incoming request is authenticated with Atlassian Connect
    app.get('/hello-world', addon.authenticate(), function (req, res) {
            // Rendering a template is easy; the `render()` method takes two params: name of template
            // and a json object to pass the context in
	    //res.download("/tmp/screen.png");
	    var fs = require('fs');
	    fs.readdir('/tmp/', function(err, files){
                 for (i = 0; i < files.length; i++) {
                     res.write('<a href=\"/download?file=' + files[i] + '\">' + files[i] + '<br>');
                 }
   		 //files.forEach(function(file){
        	 //	res.write('<a href=\"' + file + '\">' + file + '<br>');
    		 //});
                 res.end();
	    });
	   
           // res.render('hello-world', {
            //    title: 'Test Connect'
                //issueId: req.query['issueId']
          //  });
        }
    );
    // Add any additional route handlers you need for views or REST resources here...
    app.get('/download', function(req, res) {
        res.download('/tmp/' + req.query['file']);
    });
 
    app.post('/upload-file',  function (req, res) {
	file = req.files.fileToUpload;
	file.mv("/tmp/"+file.name,function(err){
	  if(err)
	    return res.status(500).send(err);

	  res.send('File uploaded !');
        });
    });
 
    
    // load any additional files you have in routes and apply those to the app
    {
        var fs = require('fs');
        var path = require('path');
        var files = fs.readdirSync("routes");
        for(var index in files) {
            var file = files[index];
            if (file === "index.js") continue;
            // skip non-javascript files
            if (path.extname(file) != ".js") continue;

            var routes = require("./" + path.basename(file));

            if (typeof routes === "function") {
                routes(app, addon);
            }
        }
    }
};
